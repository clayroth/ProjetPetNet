<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;

class messageController extends Controller
{
    public function index()
    {

    }
    public function lire($messageId)
    {
        
        $data = Message::find($messageId);
        return view('message',compact('data'));
    }
}
