<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        if(Auth::user()->accreditation == 1)
        {
            $dataAnimal = User::find(Auth::user()->id)->animals;
            return view('home',compact('dataAnimal'));
        }
        else
        {
            return view('admin');
        }
    }

    public function post(Request $request)
    {
        if(isset($request['addAnimal']))
        {
            $validator = Validator::make($request->all(),
            [
                'nom'=> 'required|min:2|max:20',
                'type'=>'required', 
                'race'=>'required',
                'naissance'=>'required|date',
            ]);
    
            if($validator->fails())
            {
                return redirect('home')->withErrors($validator)->withInput();
            }
            else
            {
                $animal = new Animal();
                $animal->user_id = Auth::user()->id;
                $animal->name = $request->input('nom');
                $animal->naissance = $request->input('naissance');
                $animal->typeAnimal = $request->input('type');
                $animal->race = $request->input('race');
                $animal->localisation = rand(10000,99999);
                if($request->input('image_animal') != null)
                    {
                        $animal->photo_profil = $request->input('image_animal');
                    }
                $animal->save();

                return redirect('home')->with(['status'=> 'Animal enregistré !']);
            }
        }

        elseif(isset($request['sendMessage']))
        {
            $validator= Validator::make($request->all(),
            [
                'sujet'=>'required',
                'message'=>'required',
            ]);

            if($validator->fails())
            {
                return redirect('home')->withErrors($validator)->withInput();
            }

            else
            {
                $getDestinataire = User::where('name','admin')->first();
                $message = new Message();
                $message->user_id= $getDestinataire->id;

                $message->sujet= $request->input('sujet');
                $message->contenu= $request->input('message');
                $message->expediteur= Auth::user()->name;
                $message->save();
                
                return redirect('home')->with(['status'=> 'message envoyé !']);
            }
        }
    }

    public function messagerie()
    {
        $dataMessage =User::find(Auth::user()->id)->messages;
        return view('messagerie',compact('dataMessage'));
    }
}
