<?php

use App\Animal;
use Illuminate\Database\Seeder;

class DataAnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Animal::class,10)->create();
    }
}
