@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Votre Espace Utilisateur</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-8 d-flex justify-content-center">
                            <table class="table m-5">
                                <tbody>
                                    <tr>
                                        <th>nom :</th>
                                        <td scope="row">{{ Auth::user()->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>email :</th>
                                        <td>{{ Auth::user()->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Compte crée le :</th>
                                        <td>{{ Auth::user()->created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                            <div class="col-sm-4">
                            <img class="img-thumbnail mb-5" width="200px" src="{{asset('img/profil/'.Auth::user()->photo_profil)}}" alt="Photo de profil">
                            
                            <form class="form-group row" action="" method="POST">
                                <input type="file" class="form-control-file m-3">
                                <a  name="" class="btn btn-primary m-3" href="#" role="button"> Modifier la photo </a>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center text-center">

        <div class="col-md-12">
                <div class="card-header mb-3 mt-5"><h3>Vos Animaux</h3></div>
                
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row justify-content-end">
                <a class="btn btn-info m-3" data-toggle="modal" data-target=".AjoutAnimal" href="#" role="button">Inscrire un animal</a>
                <a name="" id="" class="btn btn-success m-3" data-toggle="modal" data-target=".contactCentre" href="#" role="button">Contacter le centre</a>

            </div>
                <div class="card">
                    <div class="row justify-content-center p-3">
                        @if (isset($dataAnimal))
                                @foreach ($dataAnimal as $animal)
                                    <div class="col-md-4 card d-flex justify-content-center m-2"> <!-- Carte animal -->
                                    <div class="card-body">
                                    <p class="card-text"><strong>{{$animal->name}}</strong></p>
                                    </div>
                                    <div class="text-center">
                                        <img class="img-fluid img-thumbnail" width="50%" src="{{asset('img/profil/'.$animal->photo_profil)}}" alt="Card image">
                                    </div>
                                    <div class="card-body">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Type :</th>
                                                    <td scope="row">{{$animal->typeAnimal}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date de naissance :</th>
                                                    <td>{{$animal->naissance}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Race :</th>
                                                    <td>{{$animal->race}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Inscrit le : </th>
                                                    <td>{{$animal->created_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="row justify-content-center">
                                            <div class="col-sm-4">
                                                <a name=""class="btn btn-info" href="#" role="button">webcam</a>
                                            </div>
                                            <div class="col-sm-4">
                                                <a name=""class="btn btn-info" href="#" role="button">Localisation</a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                @endforeach
                        @else
                            <h1>Vous n'avez aucun animal d'enregistrer</h1>
                        @endif
                    </div>
                </div>
        </div>
    </div>
</div>

    <!-- MODAL POUR L AJOUT D UN ANIMAL -->
    <div class="modal fade AjoutAnimal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content p-5">
                <form method="POST" action="">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="Nom">Nom</label>
                    <input type="text" class="form-control" id="Nom" value="{{old('nom')}}" name="nom" placeholder="Entrer le nom de l'animal">
                    </div>
                    <div class="form-group">
                        <select class="custom-select" name="type">
                            <option selected="{{old('type')}}">Choisissez le type d'animal</option>
                            <option value="Chien">Chien</option>
                            <option value="Chat">Chat</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Race">Race</label>
                        <input type="text" name="race" value="{{old('race')}}" class="form-control" id="Race" placeholder="Entrer la race de votre animal">
                    </div>
                    <div class="form-group">
                        <label for="Naissance">Entrer la date de naissance</label>
                        <input type="date" name="naissance" value="{{old('naissance')}}" class="form-control" id="Naissance" placeholder="Entrer la date de naissance">
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="image_animal" value=" {{old('image_animal')}} " class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Chargez la photo de votre animal</label>
                        </div>                
                    </div>
                    <button type="submit" name="addAnimal" class="btn btn-primary">Enregistrer</button>
                </form>
        </div>
        </div>
    </div>

    <!--MODAL POUR CONTACTER LE CENTRE -->

    <div class="modal fade contactCentre" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content p-5">
                <form method="POST" action="home">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <label for="Sujet">Sujet</label>
                    <input type="text" value="{{old('sujet')}}" class="form-control" id="Sujet" name="sujet" placeholder="Entrer le sujet de votre message">
                    </div>
                    <div class="form-group">
                        <label for="Sujet">Message</label>
                        <textarea name="message" value="{{old('message')}}" id="Sujet" class="form-control" cols="30" rows="10" placeholder="Entre votre message"></textarea>
                    </div>
                    <button type="submit" name="sendMessage" class="btn btn-primary">Envoyer</button>
                </form>
        </div>
    </div>


@endsection
