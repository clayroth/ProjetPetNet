@extends('layouts.app')

@section('content')
        <div class="text-center bg-secondary text-light p-2 border">
            <h1 class="ml-3">Pet.Net</h1>
            <h1 class="ml-5">Plus qu'un centre, un sanctuaire ...</h1>
        </div>
        <h3 class="text-center mb-5 mt-5">nos nouveaux amis arrviants</h3>
        <div class="row justify-content-between mb-5">
            <div class="col-md-4">
                <img class="img-thumbnail" src="https://via.placeholder.com/350" alt="">
            </div>
            <div class="col-md-4">
                <img class="img-thumbnail" src="https://via.placeholder.com/350" alt="">
            </div>
            <div class="col-md-4">
                <img class="img-thumbnail" src="https://via.placeholder.com/350" alt="">
            </div>
        </div>
    <hr>

    <div class="row mb-5">
        <div class="col-md-6">
            <h2 class="text-center mb-5 mt-5">Nos services : </h2>
            <div class="card">
                <ul class="list-group list-group-flush text-center"  style="font-size: 2em">
                    <li class="list-group-item" >Garidennage</li>
                    <li class="list-group-item">Soins</li>
                    <li class="list-group-item">Dressage</li>
                    <li class="list-group-item">Toilettage</li>
                </ul>
            </div>
            
        </div>
        <div class="col-md-6">
            <iframe id="inlineFrameExample"
                title="Inline Frame Example"
                width="500"
                height="500"
                src="https://www.openstreetmap.org/export/embed.html?bbox=-0.004017949104309083%2C51.47612752641776%2C0.00030577182769775396%2C51.478569861898606&layer=mapnik">
            </iframe>
        </div>
    </div>    


    <hr>
        <h3 class="text-center mt-5">Pet Actu</h3>
        <div class="row mt-5 mb-5">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">Les nouvelles lois sur l adoptions </div>
                    <a href="#"><img class="card-img-bottom" src="https://via.placeholder.com/350" alt="Card image cap"></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">Le scandalle de Royal Canin </div>
                    <a href="#"><img class="card-img-bottom" src="https://via.placeholder.com/350" alt="Card image cap"></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">Un Covid invisible pour nos amis </div>
                    <a href="#"><img class="card-img-bottom" src="https://via.placeholder.com/350" alt="Card image cap"></a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="card p-3">
                    <h4 class="text-center mb-3 mt-3">Ancienne Infos</h4>
                    <ul class="list-group list-group-flush">
                        <a href=""><li class="list-group-item">Cras justo odio</li></a>
                        <a href=""><li class="list-group-item">Cras justo odio</li></a>
                        <a href=""><li class="list-group-item">Cras justo odio</li></a>
                        <a href=""><li class="list-group-item">Cras justo odio</li></a>
                        <a class="mt-4" href="#">Voir plus d'infos</a>
                    </ul>
                </div>
            </div>
        </div>

@endsection