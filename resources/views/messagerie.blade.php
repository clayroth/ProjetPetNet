@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>date</th>
                <th>expéditeur</th>
                <th>Sujet</th>
            </tr>
        </thead>
        <tbody>
                @foreach ($dataMessage as $message)
                <tr>
                    <td> {{$message->created_at}} </td>
                    <td>{{$message->expediteur}}</td>
                    <td>{{$message->sujet}}</td>
                    <td>
                        <a name="" class="btn btn-primary" href="/messagerie/{{$message->id}}" role="button">Lire</a>
                        <a name="" class="btn btn-danger" href="message/{{$message->id}}" role="button">Supprimer</a>
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>

@endsection