<?php

use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home','HomeController@post');
Route::get('message/{id}', function ($id) {
    $data = Message::find($id);
    $data->delete();
    return redirect('/messagerie');
});
Route::get('/messagerie','HomeController@messagerie')->name('messagerie');


Route::get('/messagerie/{messageId}','messageController@lire')->name('lire');


